<?php

namespace Core;

use mysqli;
use App\Config;

/**
 * Base model
 *
 * PHP version 7.0
 */
abstract class Model extends mysqli
{

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */

    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            $db = new mysqli(Config::DB_HOST, Config::DB_USER, Config::DB_PASSWORD, Config::DB_NAME);

            if (mysqli_connect_error()) {
                die('Ошибка подключения (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
            }
        }

        return $db;
    }
}
