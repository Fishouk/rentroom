<?php

namespace App\Controllers;

use \Core\View;
use App\Models\UserAccount as UserAccount;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class SigninController extends \Core\Controller
{
    protected $user_login, $user_password, $errors=null;

    public function __construct()
    {

    }

    public function userSigninDashboardAction() {
        if(!empty($_POST['user_login']) && !empty($_POST['user_password'])) {
            $this->user_login = $_POST["user_login"];
            $this->user_password = $_POST["user_password"];
            $result = UserAccount::userLogin($this->user_login, $this->user_password);
            if (!empty($result)) {
                $_SESSION['account'] = ['website_login' => true,
                    'user_id' => $result['id'],
                    'user_login' => $result['login'],
                    'user_email' => $result['email']];
                header('Location: /dashboard/');
            } else {
                $this->errors = "Не правильный логин и/или пароль";
            }
        }
    }

    public function errors() {
        return $this->errors;
    }

    public function indexAction()
    {
        View::renderTemplate('Main/Account/signin.html', ['SigninController' => new SigninController()]);
    }
}
