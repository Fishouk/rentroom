<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 07.04.2017
 * Time: 16:11
 */

namespace App\Controllers;

use \Core\View;

class BuildingsController extends \Core\Controller
{
    public function indexAction()
    {
        View::renderTemplate('Dashboard/Buildings/buildings.html');
    }
}