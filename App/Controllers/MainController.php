<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 08.04.2017
 * Time: 14:40
 */

namespace App\Controllers;

use \Core\View;

class MainController extends \Core\Controller
{
    public function indexAction()
    {
        View::renderTemplate('main/index.html');
    }
}