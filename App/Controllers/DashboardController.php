<?php

namespace App\Controllers;

use \Core\View;

class DashboardController extends \Core\Controller
{


    public function userLogoutAction() {
        unset($_SESSION['account']);
        session_destroy();
        header('Location: /');
    }

    public function indexAction()
    {
        View::renderTemplate('Dashboard/index.html', [ 'session' => $_SESSION ]);
    }
}