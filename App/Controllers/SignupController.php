<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 07.04.2017
 * Time: 14:55
 */

namespace App\Controllers;

use App\Models\Account as Account;
use \Core\View;

class SignupController extends \Core\Controller
{
    public function indexAction()
    {
        View::renderTemplate('Main/Account/signup.html');
    }
}