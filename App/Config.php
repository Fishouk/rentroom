<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = SETTINGS['DB_HOST'];

    /**
     * Database name
     * @var string
     */
    const DB_NAME = SETTINGS['DB_DATABASE'];

    /**
     * Database user
     * @var string
     */
    const DB_USER = SETTINGS['DB_USERNAME'];

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = SETTINGS['DB_PASSWORD'];

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}
