<?php

session_start();

/**
 *  Here global settings for website
 */
if (file_exists('.settings')) {
    define('SETTINGS', parse_ini_file( '.settings'));
} else {
    die ('No settings file');
}

/**
 * Add composer
 */
require 'vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Routing
 */
$router = new \Core\Router();

// Add the routes
$router->add('', ['controller' => 'MainController', 'action' => 'index']);
$router->add('signin/', ['controller' => 'SigninController', 'action' => 'index']);
$router->add('signup/', ['controller' => 'SignupController', 'action' => 'index']);

//Dashboard routes
$router->add('dashboard/', ['controller' => 'DashboardController', 'action' => 'index']);
$router->add('dashboard/logout/', ['controller' => 'DashboardController', 'action' => 'userLogout']);
$router->add('buildings/', ['controller' => 'BuildingsController', 'action' => 'index']);

// App starts
$router->dispatch($_SERVER['QUERY_STRING']);